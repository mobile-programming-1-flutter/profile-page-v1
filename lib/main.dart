import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

enum APP_THEME{LIGHT, DARK}

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.blueGrey.shade900,
        iconTheme: IconThemeData(
            color: Colors.blue
            ),
        ),
        iconTheme: IconThemeData(
          color: Colors.blue
        ),
        
      );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.blueGrey.shade900,
        iconTheme: IconThemeData(
            color: Colors.pink),
        ),
        iconTheme: IconThemeData(
          color: Colors.pink
        ),
        
      );
  }
}

const divider = Divider(color: Colors.black,);

var appbar = AppBar(
  
            leading: Icon(Icons.arrow_back_outlined, 
            // color: Colors.pink
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.star_border_outlined,
                  // color: Colors.pink,
                ),
              ),
            ],
          );

var body = ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    child: Image.network(
                      "https://ichef.bbci.co.uk/news/800/cpsprodpb/cbc1/live/74fe8e20-5170-11ed-ac87-630245663c6a.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    height: 60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            "Pisit Karawong",
                            style: TextStyle(fontSize: 30),
                          ),
                        ),
                      ],
                    ),
                  ),
                  divider,
                  Container(
                    margin: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Theme(
                      data: ThemeData(
                        iconTheme: IconThemeData(
                          color: Colors.pink
                        )
                      ),
                      child: profilepage()
                    ),
                  ),
                
                  Container(
                    margin: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        divider,
                        mobliePhoneListTable(), 
                        divider, 
                        otherPhoneListTable(),
                        divider,
                        emailListTable(),
                        divider,
                        addressListTable(),
                        divider,
                      ],
                    ),
                  ),
                  Divider(),
                ],
              )
            ],
          );

Widget profilepage(){
  return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        buildCallButton(),
                        buildTextButton(),
                        buildVideoCallButton(),
                        buildDirectionsButton(),
                        buildPayButton(),
                      ],)
                    ;}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: currentTheme == APP_THEME.DARK
      ?MyAppTheme.appThemeLight()
      :MyAppTheme.appThemeDark(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: appbar,
          body: body,
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.punch_clock),
            onPressed: (() {
              setState(() {
                currentTheme == APP_THEME.DARK
                ?currentTheme = APP_THEME.LIGHT
                :currentTheme = APP_THEME.DARK;
              });
            }),
          ),
          ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.text_snippet,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("VideoCall"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobliePhoneListTable() {
  return ListTile(
    leading: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.call,
                  //color: Colors.indigo.shade800,
                ),
              ),
    trailing: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.message,
                  //color: Colors.indigo.shade800,
                ),
              ),
    title: Text("061-608-2365"),
    subtitle: Text("mobile"),
  );
}

Widget otherPhoneListTable() {
  return ListTile(
    leading: Text(""),
    trailing: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.message,
                  //color: Colors.indigo.shade800,
                ),
              ),
    title: Text("121-231-1212"),
    subtitle: Text("other"),
  );
}

Widget emailListTable() {
  return ListTile(
    leading: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.email,
                  //color: Colors.indigo.shade800,
                ),
              ),
    trailing: Text(""),
    title: Text("63160209@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget addressListTable() {
  return ListTile(
    leading: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.location_on,
                  //color: Colors.indigo.shade800,
                ),
              ),
    trailing: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.directions,
                  //color: Colors.indigo.shade800,
                ),
              ),
    title: Text("711 Sukumnumleuk st. Samutprakan "),
    subtitle: Text("home"),
  );
}